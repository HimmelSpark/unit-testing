const server = require('../server.js');
const config = require('../config/index.js');
const fs = require('fs-extra');
const assert = require('assert');
const { COPYFILE_EXCL } = fs.constants;
const http = require('http');

describe('GET request', () => {

	let app;
	const INDEX_URL =  'http://localhost:3333/';
	const INDEX_PATH = './public/index.html';
	const NOT_EXISTING_FILE_URL = 'http://localhost:3333/norm.txt';

	before((done) => {
		app = server.listen(3333, () => {
			done();
		});
	});

	it('should return file index.html', function (done) {
		return http.get(INDEX_URL, (resp) => {

			let data = '';

			resp.on('error', (err) => {
				assert(false, err.getMessage());
				done();
			});

			resp.on('data', (chunk) => {
				data += chunk;
			});

			resp.on('end', () => {
				fs.readFile(INDEX_PATH, 'utf-8', (err, file) => {

					if (err) {
						assert(false);
						done();
					}

					assert(file.localeCompare(data) === 0, 'files did not match');
					done();
				})
			});

		});
	});

	it('should return status code 404 on not existing file', function (done) {

		const NOT_FOUND = 404;

		return http.get(NOT_EXISTING_FILE_URL, (resp) => {

			if (resp.statusCode !== NOT_FOUND) {
				assert(false, 'returned NOT_404 response on NOT_EXISTING resource');
				done();
			}

			done();

		});
	});

	after((done) => {
		app.close(() => {
		done();
		});
	});

});
